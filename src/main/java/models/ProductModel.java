package main.java.models;

import com.google.gson.annotations.SerializedName;



public class ProductModel {
    public static final String SERIALIZED_PROVIDER = "provider";
    @SerializedName(SERIALIZED_PROVIDER)
    public String provider;

    public static final String SERIALIZED_TITLE = "title";
    @SerializedName(SERIALIZED_TITLE)
    public String title;

    public static final String SERIALIZED_URL = "url";
    @SerializedName(SERIALIZED_URL)
    public String url;

    public static final String SERIALIZED_BRAND = "brand";
    @SerializedName(SERIALIZED_BRAND)
    public String brand;

    public static final String SERIALIZED_PRICE = "price";
    @SerializedName(SERIALIZED_PRICE)
    public String price;

    public static final String SERIALIZED_UNIT = "unit";
    @SerializedName(SERIALIZED_UNIT)
    public String unit;

    public static final String SERIALIZED_ISPROMO = "isPromo";
    @SerializedName(SERIALIZED_ISPROMO)
    public Boolean isPromo;

    public static final String SERIALIZED_PROMODETAILS = "promoDetails";
    @SerializedName(SERIALIZED_PROMODETAILS)
    public String promoDetails;

    public static final String SERIALIZED_IMAGE = "image";
    @SerializedName(SERIALIZED_IMAGE)
    public String image;
}
