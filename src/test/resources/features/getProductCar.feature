Feature: Search for not existing product car

  Scenario:Retrieve product by not existing product name
    Given client calls endpoint PRODUCT_API for "car"
    Then for "car" product endpoint, the client should receive an HTTP 404 response code