Feature: Search for the product mango

 Scenario: Retrieve product by product name
    Given client calls endpoint PRODUCT_API for "mango"
    Then for "mango" product endpoint, the client should receive an HTTP 200 response code
	 And verify the response received for product "mango"

 Scenario: Retrieve product by invalid product name
	 Given client tries to fetch product with any invalid "<script>" name
	 Then Error should be shown as product is invalid
	 And  for "<script>" status code should be "404"

 Scenario:Retrieve product for specific distributor by product name
	 Given client tries to fetch "mango"
	 Then count of "mango" products for distributor "Vomar" should be more than 1
	 And verify the response received for product "mango"
