package test.java.action;

import lombok.extern.log4j.Log4j2;
import main.java.models.ProductModel;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import test.java.utils.Endpoints;
import org.junit.Assert;
import java.util.List;
import java.util.Map;

@Log4j2
public class ProductAction {

public static String responseBody;

	public static void validate_get_api_status_code (String product, Integer code) {
		log.info ("Validating status code ");
		SerenityRest.when ().get (Endpoints.PRODUCT_API + product).then ().assertThat ().statusCode (code);
	}


	public static void get_product_count_as_per_distributor ( String product, String distributor, Integer count ) throws JsonProcessingException {
		log.info ("Validating product count per distributor");
		Object value = null;
		double cnt = (double) 0;
		responseBody = SerenityRest.when ().get (Endpoints.PRODUCT_API + product).then ().contentType (ContentType.JSON).extract ().response ().asString ();

		List<Map<String, String>> responseList = (List<Map<String, String>>) new ObjectMapper ().readValue (responseBody, new TypeReference<List<Map<String, String>>> () {
		});

		for (int i = 0; i < responseList.size (); i++) {
			Map<String, String> pair = ((Map<String, String>) responseList.get (i));
			value = pair.get ("provider");
			if (value.toString ().equalsIgnoreCase (distributor)) {
				cnt++;
			}
		}
		Assert.assertTrue ("product count greater than 1", (double) cnt > count.doubleValue ());
	}

	public static ProductModel getBody( String product) throws JsonProcessingException {
	log.info("Get response body");
		ObjectMapper objectMapper = new ObjectMapper();
		List<ProductModel> ProductMangoList = objectMapper.readValue(SerenityRest.when().get(Endpoints.PRODUCT_API+product).asString(), new TypeReference<List<ProductModel>>() {});
		return ProductMangoList.get(0);
	}
}
