package test.java.stepdefinitions;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j2;
import main.java.models.ProductModel;
import test.java.action.ProductAction;


@Log4j2
public class ProductSteps {

	@When("^client calls endpoint PRODUCT_API for \"([^\"]*)\"$")
	public void clientCallsEndpointFor(String arg1) throws Throwable {
	}

	@Then("^for \"([^\"]*)\" product endpoint, the client should receive an HTTP (\\d+) response code$")
	public void forProductEndpointTheClientShouldReceiveAnHTTPResponseCode(String arg1, int arg2) throws Throwable {
		ProductAction.validate_get_api_status_code(arg1, arg2);
	}


	@Given("^client tries to fetch product with any invalid \"([^\"]*)\" name$")
	public void clientTriesToFetchProductWithAnyInvalidName(String arg1) throws Throwable {

	}

	@Then("^Error should be shown as product is invalid$")
	public void errorShouldBeShownAsProductIsInvalid() throws Throwable {

	}

	@And("^for \"([^\"]*)\" status code should be \"([^\"]*)\"$")
	public void forStatusCodeShouldBe(String arg1, int arg2) throws Throwable {
		ProductAction.validate_get_api_status_code(arg1, arg2);
	}

	@Given("^client tries to fetch \"([^\"]*)\"$")
	public void clientTriesToFetch(String arg1) throws Throwable {

	}


	@Then("^count of \"([^\"]*)\" products for distributor \"([^\"]*)\" should be more than (\\d+)$")
	public void countOfProductsForShouldBe(String arg1, String arg2, int arg3) throws Throwable {
		ProductAction.get_product_count_as_per_distributor(arg1, arg2, arg3);
	}

	@And("verify the response received for product \"([^\"]*)\"$")
	public void verifyTheResponseReceivedForProductMango(String arg1) throws JsonProcessingException {
		log.info("Verifying response of GET product by name ");
		ProductModel responseBody = ProductAction.getBody(arg1);
		assertNotNull(responseBody.provider);
		assertNotNull(responseBody.title);
		assertNotNull(responseBody.url);
		assertNotNull(responseBody.brand);
		assertNotNull(responseBody.price);
		assertNotNull(responseBody.unit);
		assertNotNull(responseBody.isPromo);
		assertNotNull(responseBody.promoDetails);
		assertNotNull(responseBody.image);
	}
}
