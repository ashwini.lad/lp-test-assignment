# Getting started with Serenity and Cucumber

Serenity BDD is a library that makes it easier to write high quality automated acceptance tests, with powerful reporting and living documentation features. It has strong support for both web testing with Selenium, and API testing using RestAssured.

Serenity strongly encourages good test automation design, and supports several design patterns, including classic Page Objects, the newer Lean Page Objects/ Action Classes approach, and the more sophisticated and flexible Screenplay pattern.

The latest version of Serenity supports Cucumber 5.5.

## The leaseplan assignment project
This project is created after cleaning up the starter porject and this gives you a basic project setup, along with some sample tests and supporting classes. 

### The project directory structure
The project has build scripts for Maven and follows the standard directory structure used in most Serenity projects:
```Gherkin
src
  + main
    + java
      + Models                    Models here
  + test
    + java                        Test runners and supporting code
    + resources
      + features                  Feature files
          getProductApple.feature
          getProductCar.feature
          getProductMango.feature
```
## The test scenario's
getProductApple
```Gherkin
Feature: Search for the product apple

  Scenario: Retrieve product by product name
    Given client calls endpoint PRODUCT_API for "apple"
    Then for "apple" product endpoint, the client should receive an HTTP 200 response code
    And verify the response received for product "apple"

  Scenario: Retrieve product by invalid product name
    Given client tries to fetch product with any invalid "<script>" name
    Then Error should be shown as product is invalid
    And  for "<script>" status code should be "404"

  Scenario:Retrieve product for specific distributor by product name
    Given client tries to fetch "apple"
    Then count of "apple" products for distributor "Vomar" should be more than 1
    And verify the response received for product "apple"
```
getProductCar
```Gherkin
Feature: Search for not existing product car

  Scenario:Retrieve product by not existing product name
    Given client calls endpoint PRODUCT_API for "car"
    Then for "car" product endpoint, the client should receive an HTTP 404 response code
```
getProductMango
```Gherkin
Feature: Search for the product mango

  Scenario: Retrieve product by product name
    Given client calls endpoint PRODUCT_API for "mango"
    Then for "mango" product endpoint, the client should receive an HTTP 200 response code
    And verify the response received for product "mango"

  Scenario: Retrieve product by invalid product name
    Given client tries to fetch product with any invalid "<script>" name
    Then Error should be shown as product is invalid
    And  for "<script>" status code should be "404"

  Scenario:Retrieve product for specific distributor by product name
    Given client tries to fetch "mango"
    Then count of "mango" products for distributor "Vomar" should be more than 1
    And verify the response received for product "mango"
```

### The Action Class implementation.
```java
	public static void validate_get_api_status_code (String product, Integer code) {
        log.info ("Validating status code ");
        SerenityRest.when ().get (Endpoints.PRODUCT_API + product).then ().assertThat ().statusCode (code);
        }
        
public static void get_product_count_as_per_distributor ( String product, String distributor, Integer count ) throws JsonProcessingException {
        log.info ("Validating product count per distributor");
        Object value = null;
        double cnt = (double) 0;
        responseBody = SerenityRest.when ().get (Endpoints.PRODUCT_API + product).then ().contentType (ContentType.JSON).extract ().response ().asString ();

        List<Map<String, String>> responseList = (List<Map<String, String>>) new ObjectMapper ().readValue (responseBody, new TypeReference<List<Map<String, String>>> () {
        });

        for (int i = 0; i < responseList.size (); i++) {
        Map<String, String> pair = ((Map<String, String>) responseList.get (i));
        value = pair.get ("provider");
        if (value.toString ().equalsIgnoreCase (distributor)) {
        cnt++;
        }
        }
        Assert.assertTrue ("product count greater than 1", (double) cnt > count.doubleValue ());
        }

public static ProductModel getBody( String product) throws JsonProcessingException {
        log.info("Get response body");
        ObjectMapper objectMapper = new ObjectMapper();
        List<ProductModel> ProductMangoList = objectMapper.readValue(SerenityRest.when().get(Endpoints.PRODUCT_API+product).asString(), new TypeReference<List<ProductModel>>() {});
        return ProductMangoList.get(0);
        }
```
### The Stepdefinition Class implementation.
```java
@When("^client calls endpoint PRODUCT_API for \"([^\"]*)\"$")
public void clientCallsEndpointFor(String arg1) throws Throwable {
        }

@Then("^for \"([^\"]*)\" product endpoint, the client should receive an HTTP (\\d+) response code$")
public void forProductEndpointTheClientShouldReceiveAnHTTPResponseCode(String arg1, int arg2) throws Throwable {
        ProductAction.validate_get_api_status_code(arg1, arg2);
        }


@Given("^client tries to fetch product with any invalid \"([^\"]*)\" name$")
public void clientTriesToFetchProductWithAnyInvalidName(String arg1) throws Throwable {

        }

@Then("^Error should be shown as product is invalid$")
public void errorShouldBeShownAsProductIsInvalid() throws Throwable {

        }

@And("^for \"([^\"]*)\" status code should be \"([^\"]*)\"$")
public void forStatusCodeShouldBe(String arg1, int arg2) throws Throwable {
        ProductAction.validate_get_api_status_code(arg1, arg2);
        }

@Given("^client tries to fetch \"([^\"]*)\"$")
public void clientTriesToFetch(String arg1) throws Throwable {

        }


@Then("^count of \"([^\"]*)\" products for distributor \"([^\"]*)\" should be more than (\\d+)$")
public void countOfProductsForShouldBe(String arg1, String arg2, int arg3) throws Throwable {
        ProductAction.get_product_count_as_per_distributor(arg1, arg2, arg3);
        }

@And("verify the response received for product \"([^\"]*)\"$")
public void verifyTheResponseReceivedForProductMango(String arg1) throws JsonProcessingException {
        log.info("Verifying response of GET product by name ");
        ProductModel responseBody = ProductAction.getBody(arg1);
        assertNotNull(responseBody.provider);
        assertNotNull(responseBody.title);
        assertNotNull(responseBody.url);
        assertNotNull(responseBody.brand);
        assertNotNull(responseBody.price);
        assertNotNull(responseBody.unit);
        assertNotNull(responseBody.isPromo);
        assertNotNull(responseBody.promoDetails);
        assertNotNull(responseBody.image);
        }
```
### The ProductModel Class .
```java
public static final String SERIALIZED_PROVIDER = "provider";
    @SerializedName(SERIALIZED_PROVIDER)
    public String provider;

    public static final String SERIALIZED_TITLE = "title";
    @SerializedName(SERIALIZED_TITLE)
    public String title;

    public static final String SERIALIZED_URL = "url";
    @SerializedName(SERIALIZED_URL)
    public String url;

    public static final String SERIALIZED_BRAND = "brand";
    @SerializedName(SERIALIZED_BRAND)
    public String brand;

    public static final String SERIALIZED_PRICE = "price";
    @SerializedName(SERIALIZED_PRICE)
    public String price;

    public static final String SERIALIZED_UNIT = "unit";
    @SerializedName(SERIALIZED_UNIT)
    public String unit;

    public static final String SERIALIZED_ISPROMO = "isPromo";
    @SerializedName(SERIALIZED_ISPROMO)
    public Boolean isPromo;

    public static final String SERIALIZED_PROMODETAILS = "promoDetails";
    @SerializedName(SERIALIZED_PROMODETAILS)
    public String promoDetails;

    public static final String SERIALIZED_IMAGE = "image";
    @SerializedName(SERIALIZED_IMAGE)
    public String image;
```

### Endpopints class
We can also configure endpoints properties
```java
public static String PRODUCT_API = "https://waarkoop-server.herokuapp.com/api/v1/search/test/";
```

## Executing the tests
To run the project, you can either just run the `TestRunner` class, or run either `mvn verify` from the command line.
```
$ mvn clean verify
```

## Test Results Serenity report
The test results will be recorded in the `target/site/serenity` directory.
Click on index.html, which has all the referencers to other pages of reports

## Generating the reports
Since the Serenity reports contain aggregate information about all of the tests, they are not generated after each individual test (as this would be extremenly inefficient). Rather, The Full Serenity reports are generated by the `serenity-maven-plugin`. You can trigger this by running `mvn serenity:aggregate` from the command line or from your IDE.

They reports are also integrated into the Maven build process: the following code in the `pom.xml` file causes the reports to be generated automatically once all the tests have completed when you run `mvn verify`?

```
<plugin>
    <groupId>net.serenity-bdd.maven.plugins</groupId>
    <artifactId>serenity-maven-plugin</artifactId>
    <version>3.1.15</version>
    <dependencies>
        <dependency>
            <groupId>net.serenity-bdd</groupId>
            <artifactId>serenity-single-page-report</artifactId>
            <version>3.1.15</version>
            </dependency>
            <dependency>
            <groupId>net.serenity-bdd</groupId>
            <artifactId>serenity-navigator-report</artifactId>
            <version>3.1.15</version>
        </dependency>
    </dependencies>
    <configuration>
    <tags></tags>
    <reports>single-page-html,navigator</reports>
    </configuration>
    <executions>
    <execution>
    <id>serenity-reports</id>
    <phase>post-integration-test</phase>
    <goals>
        <goal>aggregate</goal>
    </goals>
    </execution>
    </executions>
</plugin>
```

## Run CI/CD pipeline
add .getlab-ci.xml file with following commands for 3 stage pipeline build, test and reports. Reports will stay for 1 week and then those will be deleted
```
image: maven:3.3-jdk-8

cache:
  paths:
    - .m2/repository/
    - target/

build-leaseplanassignment:
  stage: build
  script:
    - echo "This job build leaseplan assignment automation Project."
    - mvn clean

test-leaseplanassignment:
  stage: test
  script:
    - echo "This job run testcases of leaseplan assignment automation Project."
    - mvn verify
    - echo "This job copy test report of leaseplan assignment automation Project to public directory."
    - mkdir public
    - mv target/site/serenity/* public
  artifacts:
    when: always
    paths:
      - public
    expire_in: 1 week
```
